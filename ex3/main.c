/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on January 24, 2018, 11:26 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*
 * 
 */


void getDays(char*** days){
    char d[][]={"sun", "mon","tue","wed","thu","fri","say"};
    //char a[] = "aaaa";
    //a[1] = 'b';
    //*(d[2] + 1) = 't';
    *days = malloc(7*sizeof(char*));
    for (int i = 0; i < 7; i++) {
        *(*days + i) = (char*)malloc(sizeof(char)*(strlen(d[i]) + 1));
        strcpy(*(*days + i), d[i]);
    }
}

int main(int argc, char** argv) {
    char** days;
    getDays(&days);
    for(int i = 0; i < 7; i++){
        printf("%s\n", days[i]);
        free(days[i]);
    }
    free(days);
    return (EXIT_SUCCESS);
}