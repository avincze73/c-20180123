/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on January 24, 2018, 3:46 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


struct Employee{
    int id;
    char name[50];
    char* title;
};

struct Employee* database = NULL;
int size = 0;
int top = 0;
/*
 * 
 */

void insert(const struct Employee* employee){
    struct Employee e;
    e.id = employee->id;
    strcpy(e.name, employee->name);
    e.title = (char*)malloc(sizeof(char) * (strlen(employee->title) + 1));
    strcpy(e.title, employee->title);
    
    if(top == size){
        struct Employee * temp = malloc(sizeof(struct Employee) * (2*size + 1));
        for(int i = 0; i < size; i++){
            temp[i] = database[i];
            //temp[i].id = database[i].id;
            //strcpy(temp[i].name, database[i].name);
            //temp[i].title = database[i].title;
        }
        free(database);
        database = temp;
        size = 2*size + 1;
    }
    //++top;
    database[top] = e;
    ++top;
    //database[top].id = employee->id;
    //strcpy(database[top].name, employee->name);
    //database[top].title = (char*)malloc(sizeof(char) * (strlen(employee->title) + 1));
    //strcpy(database[top].title, employee->title);
}

void show(){
    for(int i = 0; i < top; i++) {
        printf("[%d, %s, %s]\n", database[i].id, database[i].name, database[i].title);
    }
}


int main(int argc, char** argv) {

    insert(&((struct Employee){.id = 1, .name = "a1", .title = "b1"}));
    //show();
    insert(&((struct Employee){.id = 2, .name = "a2", .title = "b2"}));
    //show();
    insert(&((struct Employee){.id = 3, .name = "a3", .title = "b3"}));
    //show();
    insert(&((struct Employee){.id = 4, .name = "a4", .title = "b4"}));
    
    show();
    return (EXIT_SUCCESS);
}

