/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on January 24, 2018, 2:02 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "A.h"
#include "B.h"
/*
 * 
 */

int stringCompare(const void* s1, const void* s2){
    return *((char*)s1) - *((char*)s2);
}

bool isAnagram(const char* s1, const char* s2){
    //printf("%d", sizeof(s1));
    char* ss1 = (char*) malloc(sizeof(char)*(strlen(s1)+1));
    strcpy(ss1, s1);
    char* ss2 = (char*) malloc(sizeof(char)*(strlen(s2)+1));
    strcpy(ss2, s2);
    qsort(ss1, strlen(ss1), sizeof(char), &stringCompare);
    qsort(ss2, strlen(ss2), sizeof(char), stringCompare);
    bool ret = strcmp(ss1, ss2) == 0 ? true : false;
    free(ss1);
    free(ss2);
    return ret;
}

int main(int argc, char** argv) {
    
    struct A a;
    struct B b;
    printf("%s\n", isAnagram("securesssssss","rescusesssss") ? "yes":"no");
    return (EXIT_SUCCESS);
}

