/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   B.h
 * Author: avincze
 *
 * Created on January 24, 2018, 3:29 PM
 */




#ifndef B_H
#define B_H
#include "A.h"

struct B {
    struct A aa;
    int i;
    int j;
};

#endif