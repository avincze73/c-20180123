/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on January 24, 2018, 1:10 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
//https://gitlab.com/avincze73/c-20180123
int accum(int* a, int length, int init){
    int temp = 0;
    for (int i = 0; i < length; i++) {
        temp+=a[i];
    }
    return temp + init;
}


int Add(int a, int b){
    return a + b;
}

int Multiply(int a, int b){
    return a * b;
}

bool Even(int a){
    return a % 2 == 0;
}

bool Negative(int a){
    return a < 0;
}

int accum2(const int* begin, const int* end, int init,
        int(*policy)(int, int),
        bool(*predicate)(int)){
    for(;begin!=end;++begin){
        if (predicate(*begin)){
            init = policy(init, *begin);
        }
    }
    return init;
}

int main(int argc, char** argv) {
    int a[]={1,2,3,4,5,6,7,8,9,10};
    printf("%d\n", accum(a, 10, 0));
    printf("%d\n", accum2(a, a+10, 0, Add, Even));
    printf("%d\n", accum2(a, a+10, 1, Multiply, Negative));
    
    
    return (EXIT_SUCCESS);
}