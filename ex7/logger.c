/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "logger.h"
#include <stddef.h>
#include <stdbool.h>
static LOGGER_TYPE registry[100];
static bool initialized = false;

static void init(){
    int i;
    for (i = 0; i < 100; i++) {
        registry[i]=NULL;
    }

}
void LOG(const char* message, Severity severity){
    if(!initialized){
        init();
        initialized = true;
    }
    for (int i = 0; i < 100; i++) {
        if(registry[i] != NULL){
            registry[i](message, severity);
        }
    }
}

void attach(LOGGER_TYPE type){
    if(!initialized){
        init();
        initialized = true;
    }
    for (int i = 0; i < 100; i++) {
        if(registry[i]==NULL){
            registry[i]=type;
            break;
        }
    }
}

void detach(LOGGER_TYPE type){
    for (int i = 0; i < 100; i++) {
        if(registry[i]==type){
            registry[i]=NULL;
            break;
        }
    }
}