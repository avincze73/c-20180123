/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   logger.h
 * Author: avincze
 *
 * Created on 2018. január 26., 10:13
 */

#ifndef LOGGER_H
#define LOGGER_H


typedef enum {INFO, ERROR} Severity;
    
typedef void (*LOGGER_TYPE)(const char*, Severity) ;    
    
void LOG(const char*, Severity);

void attach(LOGGER_TYPE);

void detach(LOGGER_TYPE);

#endif /* LOGGER_H */

