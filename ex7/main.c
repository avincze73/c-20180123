/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on 2018. január 26., 10:12
 */

#include <stdio.h>
#include <stdlib.h>
#include "logger.h"

/*
 * 
 */

void consoleLogger1(const char* message, Severity severity){
    printf("LOGGER1 %s: %s\n", severity == INFO ? "INFO": "ERROR", message);
}

void consoleLogger2(const char* message, Severity severity){
    printf("LOGGER2 %s: %s\n", severity == INFO ? "INFO": "ERROR", message);    
}


int main(int argc, char** argv) {

    attach(consoleLogger1);
    attach(consoleLogger2);
    LOG("my error 1", ERROR);
    LOG("my info 1", INFO);
    
    detach(consoleLogger1);
    LOG("my error 2", ERROR);
    LOG("my info 2", INFO);
    
    
    return (EXIT_SUCCESS);
}

