/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   StringUtil.h
 * Author: avincze
 *
 * Created on January 26, 2018, 11:43 AM
 */

#ifndef STRINGUTIL_H
#define STRINGUTIL_H

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

    bool isMirror(const char* word);
    
    
#ifdef __cplusplus
}
#endif

#endif /* STRINGUTIL_H */

