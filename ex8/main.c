/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: avincze
 *
 * Created on January 26, 2018, 11:42 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include "PrintUtil.h"
#include "StringUtil.h"

/*
 * 
 */
int main(int argc, char** argv) {

    printN("Test is started...\n");
    printf("%s is %s mirror\n", "aabbaa", isMirror("aabbaa")? "" : "not");
    printN("Test is finished...\n");
    return (EXIT_SUCCESS);
}

