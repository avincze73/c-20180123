/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "StringUtil.h"
#include <string.h>



bool isMirror(const char* word){
    for (int i = 0, j = strlen(word); i < j; i++, j--) {
        if(word[i] != word[j]){
            return false;
        }
    }
    return true;
}