/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PrintUtil.h
 * Author: avincze
 *
 * Created on January 26, 2018, 11:43 AM
 */

#ifndef PRINTUTIL_H
#define PRINTUTIL_H

#ifdef __cplusplus
extern "C" {
#endif

    void printN(const char* message);


#ifdef __cplusplus
}
#endif

#endif /* PRINTUTIL_H */

